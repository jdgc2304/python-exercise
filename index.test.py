import unittest
from index import find_pair


class TestFindPair(unittest.TestCase):
    def test_find_pair(self):
        self.assertEqual(find_pair([2, 3, 6, 7], 9), "Ok, matching pair: (2, 7)")
        self.assertEqual(find_pair([1, 3, 3, 7], 9), "No matching pair.")


if __name__ == "__main__":
    unittest.main()
