def find_pair(numbers, target_sum):
    left = 0
    right = len(numbers) - 1

    while left < right:
        current_sum = numbers[left] + numbers[right]

        if current_sum == target_sum:
            return f"Ok, matching pair: ({numbers[left]}, {numbers[right]})"
        elif current_sum < target_sum:
            left += 1
        else:
            right -= 1

    return f"No matching pair."


# The function will still work even if it receives a million numbers.
# This is because the function uses a two-pointer technique,
# which is a very efficient method for solving this problem.
# The time complexity of this method is O(n), where n is the number of elements in the input list.
# This means that the time it takes to run the function increases linearly with the size of the input,
# so it can handle large inputs like a list of a million numbers.
