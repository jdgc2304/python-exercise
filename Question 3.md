# This doc provides the requested information for question #3.

This is actually a really tough one for me as I have been working in companies where all of my code is their property and therefore is considered confidential information. So, much of the code I can provide may not show current best practices because I do not maintain a personal portfolio and always use the organization's repository that is assigned to me. However I will try to be as descriptive as possible regarding this. If you need to see the code, I'm willing to show it in a call and talk about the practices I use in my current job.

I believe every project or task must first be completely understood by all members of the team to have a shared vision of what necessity it should suffice. This will give insights on what tools to use and solve the problem better than any particular tool or workflow. Once the team has this clearly defined shared goal, tasks are bucketed and assigned accordingly.

Regarding the control version I use, is Git with GitHub or Gitlab, I am used to work in protected environments and private repositories using them and a Microsoft Azure suite to handle projects. For commits I follow the conventional commits specification: https://www.conventionalcommits.org/en/v1.0.0/

## Backend Projects

### NodeJS

For backend projects written in `Javascript` or `Typescript` I tend to create 2 folders in my project, a `src` folder and a `test` folder for my unit testing (`Jest` / `Supertest`). the src is structured as follows:

- Controllers: to store the 'business' logic of my app (CRUD). They are used by my defined 'Endpoints' in the routes file.

- database: to handle db connection and dev commands (sync database, drop database...).

- errors: for custom error declarations and usage throughout my app. This way I can define errors and keep my app running even if there was an exception, as I can handle it and determine what message should I give the user depending on the error itself.

- middlewares: to store auth checking, role checking, sanitization, request limiting, or error handling.

- models: to create and sync my database models (with ODMs like mongoose for MongoDB or ORM like sequelize for SQL databases)

- utils: to store every other helper function or module and keep the structure simple enough.

All configuration is stored in a config file (environment variables, names or certain entities).

Generally I keep an index of every major folder where I import / export useful modules and define relations and maintain and easily traceable schema.

### Python

For backend projects written in Python using Django I prefer to go with this:

- Core

- Api

- Bookmarks

- Comments

- Photos

- Weblog

- Users

One of the benefits is that using this method the code is more quickly understandable by anyone looking at it for the first time.

## Frontend Projects

For frontend projects I typically use React with Redux and Typescript, preferably Vite-React and its ecosystem. I use the scalable and proven structure of bulletproof react: https://github.com/alan2207/bulletproof-react/blob/master/docs/project-structure.md depending on the project's requirements. Normally is even simpler and smaller.
