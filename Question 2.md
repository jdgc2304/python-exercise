# This doc provides the requested information for question #2.

Continous integration is a practice in software development where developers integrate their changes into a shared repository frequently. Each integration can then be verified by an automated build and automated tests.

That in itself makes it a powerful and widely used tool or work mechanism as it helps catch errors faster and give customers regular updates through continous deployment.

There are existing tools that make this continous integration / deployment way easier. Particularly, I have used Jenkins / GitHub Actions both in DevOps environments to automate such proccess.

For example:

```
pipeline {
    agent any

    stages {
        stage('Build') {
            steps {
                echo 'Building...'
                // Commands to build the project
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
                // Commands to test the project
            }
        }
        stage('Deploy') {
            steps {
                echo 'Deploying...'
                // Commands to deploy the project
            }
        }
    }
}

```

Given the simplicity of the project, I will not be setting a pipeline for this.
